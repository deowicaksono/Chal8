const express = require("express");
const app = express()
const port = 8000
const swaggerUI= require('swagger-ui-express')
const swaggerJSON= require('./swagger.json')

app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON))



app.listen(port, () => {
    console.log('server berjalan di port', port);
})