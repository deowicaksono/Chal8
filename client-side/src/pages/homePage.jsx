import React from "react";
import RegisterPlayerComponent from "../components/registerPlayerComponent";
import EditPlayerComponent from "../components/editPlayerComponent";
import FindPlayerComponent from "../components/findPlayerComponent";

function HomePage(){
    return(
        <>
            {/* <RegisterPlayerComponent/> */}
            {/* <EditPlayerComponent/> */}
            <FindPlayerComponent/>
        </>
    )
}

export default HomePage;