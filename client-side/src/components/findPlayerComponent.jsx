import React, {useState} from "react";


export default function FindPlayerComponent(){
    let [username,setUsername]= useState(null)
    let [password,setPassword]= useState(null)
    let [experience, setExperience]= useState(null)
    let [lvl, setLvl]= useState(null)

    let [submitUsername,setSubmitUsername]= useState(null)
    let [submitPassword,setSubmitPassword]= useState(null)
    let [submitExperience, setSubmitExperience]= useState(null)
    let [submitLvl, setSubmitLvl]= useState(null)
    
    let handleSubmit= () =>{
        if(username===""){
            setSubmitUsername(null)
        }else{
            setSubmitUsername(username);
        }
        if(password===""){
            setSubmitPassword(null)
        }else{
            setSubmitPassword(password);
        }
        if(experience===""){
            setSubmitExperience(null)
        }else{
            setSubmitExperience(experience);
        }
        if(lvl===""){
            setSubmitLvl(null)
        }else{
            setSubmitLvl(lvl);
        }

    }

    return(
        <>
            <h1>form Find Player</h1>
            <div className="form-group">
                <div>
                    <p>Username</p>
                    <input type='text' name='username' className="form-control" placeholder="Input username" onChange={e => setUsername(e.target.value)}></input>
                </div>
                <div>
                    <p>password</p>
                    <input type='password' name='password' className="form-control" placeholder='Input password' onChange={e => setPassword(e.target.value)}></input>
                </div>
                <div>
                    <p>Experience</p>
                    <input type='text' name='experience' className="form-control" placeholder="input experience" onChange={e => setExperience(e.target.value)}></input>
                </div>
                <div>
                    <p>Level</p>
                    <input type='text' name='id' className="form-control" placeholder="input Level" onChange={e => setLvl(e.target.value)}></input>
                </div>
                <button type="submit" onClick={handleSubmit}>Find Player</button>
            </div>
            <h2>hasil input user</h2>
            <h5>username: {username}</h5>
            <h5>password: {password}</h5>
            <h5>experience: {experience}</h5>
            <h5>level: {lvl}</h5>

            <h2> yang disubmit</h2>
            <div>
            {   
                submitUsername !== null ? (<h5>username: {submitUsername}</h5>) : (<p></p>) 
            }
            {   
                submitPassword !== null ? (<h5>Password: {submitPassword}</h5>) : (<p></p>) 
            }
            {   
                submitExperience !== null ? (<h5>Experience: {submitExperience}</h5>) : (<p></p>) 
            }
            {   
                submitLvl !== null ? (<h5>Level: {submitLvl}</h5>) : (<p></p>) 
            }
            </div>

            
        </>
    )
}