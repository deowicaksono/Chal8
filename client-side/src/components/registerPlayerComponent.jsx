import React, {useState} from "react";


export default function RegisterPlayerComponent(){
    let [username,setUsername]= useState(null)
    let [password,setPassword]= useState(null)

    let [submitUsername,setSubmitUsername]= useState(null)
    let [submitPassword,setSubmitPassword]= useState(null)
    
    let handleSubmit= () =>{
        setSubmitUsername(username);
        setSubmitPassword(password);

    }

    return(
        <>
            <h1>form Register</h1>
            <div className="form-group">
                <div>
                    <p>Username</p>
                    <input type='text' className="form-control" name='username' placeholder="Input username" onChange={e => setUsername(e.target.value)}></input>
                </div>
                <div>
                    <p>password</p>
                    <input type='password' className="form-control" name='password' placeholder='Input password' onChange={e => setPassword(e.target.value)}></input>
                </div>
                <button type="submit" onClick={handleSubmit} className="btn btn-primary">Register</button>
            </div>
                <h2>hasil input user</h2>
                <h5>username: {username}</h5>
                <h5>password: {password}</h5>
            {
                submitUsername ==null ? (
                    <p></p>
                ) : (
                    <div>

                    <h2>yang disubmit:</h2>
                    <h5>username: {submitUsername}</h5>
                    <h5>password: {submitPassword}</h5>
                    </div>
                )
            }
        </>
    )
}