import React, {useState} from "react";


export default function EditPlayerComponent(){

    let [username,setUsername]= useState(null)
    let [password,setPassword]= useState(null)
    let [id,setId]= useState(null)

    let [submitUsername,setSubmitUsername]= useState(null)
    let [submitPassword,setSubmitPassword]= useState(null)
    let [submitId, setSubmitId]=useState(null)
    
    let handleSubmit= () =>{
        setSubmitUsername(username);
        setSubmitPassword(password);
        setSubmitId(id);

    }

    return(
        <>
            <h1>form Edit</h1>
            <div className="form-group"> 
                <div>
                    <p>ID</p>
                    <input type='text' name='id' className="form-control" placeholder="input ID yang akan diedit" onChange={e => setId(e.target.value)}></input>
                </div>
                <div>
                    <p>Username</p>
                    <input type='text' name='username' className="form-control" placeholder="Input username" onChange={e => setUsername(e.target.value)}></input>
                </div>
                <div>
                    <p>password</p>
                    <input type='password' name='password' className="form-control" placeholder='Input password' onChange={e => setPassword(e.target.value)}></input>
                </div>
                <button type="submit" onClick={handleSubmit}>Submit</button>
            </div>
            <h2>hasil input user</h2>
            <h5>ID: {id}</h5>
            <h5>username: {username}</h5>
            <h5>password: {password}</h5>
            {
                submitId ==null ? (
                    <p></p>
                ) : (
                    <div>

                    <h2>yang disubmit:</h2>
                    <h5>ID: {submitId}</h5>
                    <h5>username: {submitUsername}</h5>
                    <h5>password: {submitPassword}</h5>
                    </div>
                )
            }
            
        </>
    )
}